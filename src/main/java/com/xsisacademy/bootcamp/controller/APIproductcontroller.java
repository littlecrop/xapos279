package com.xsisacademy.bootcamp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.bootcamp.model.Product;
import com.xsisacademy.bootcamp.model.Variant;
import com.xsisacademy.bootcamp.repository.ProductRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class APIproductcontroller {

	@Autowired
	public ProductRepository reposP;

	@GetMapping("product")
	public ResponseEntity<List<Product>> getAllProduct() {
		try {
			List<Product> product = this.reposP.findAll();
			return new ResponseEntity<>(product, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("varbycat/{id}")
	public ResponseEntity<List<Variant>> getVarByCat(@PathVariable("id") Long id){
		try {
			List<Variant> variant = this.reposP.findVariantByCategory(id);
			return new ResponseEntity<>(variant, HttpStatus.OK);
		}catch(Exception e){
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("add/product")
	public ResponseEntity<Object> saveProduct(@RequestBody Product product) {
		product.setpStatus(true);
		Product productdata = this.reposP.save(product);
		if (productdata.equals(product)) {
			return new ResponseEntity<Object>("Data Saved Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Data Saved Unseccessfully", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("product/{id}")
	public ResponseEntity<List<Product>> getProductById(@PathVariable("id") Long id) {
		try {
			Optional<Product> product = this.reposP.findById(id);
			if (product.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(product, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Product>>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("edit/product/{id}")
	public ResponseEntity<Object> updateProduct(@RequestBody Product product, @PathVariable("id") Long id) {
		try {
			Optional<Product> productdata = this.reposP.findById(id);
			if (productdata.isPresent()) {
				product.setId(id);
				product.setpStatus(true);
				this.reposP.save(product);
				ResponseEntity rest = new ResponseEntity<>("Data Updated Successfully", HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("delete/product/{id}")
	public ResponseEntity<Object> deleteProduct(@PathVariable("id") Long id) {
		Optional<Product> productdata = this.reposP.findById(id);
		if (productdata.isPresent()) {
			Product product = new Product();
			product.setId(id);
			this.reposP.deleteProductById(id);
			ResponseEntity rest = new ResponseEntity<>("Data Deleted Successfully", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}
	}
}
