package com.xsisacademy.bootcamp.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.bootcamp.model.OrderDetail;
import com.xsisacademy.bootcamp.repository.OrderDetailRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class APIorderdetailcontroller {
	
	@Autowired
	private OrderDetailRepository reposOD;
	
	@GetMapping("orderdetailbyorderheader/{id}")
	public ResponseEntity<List<OrderDetail>> getAllOrderById(@PathVariable("id") Long id){
		try {
			List<OrderDetail> od = this.reposOD.findByHeaderId(id);
			return new ResponseEntity<List<OrderDetail>>(od, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<List<OrderDetail>>(HttpStatus.NO_CONTENT);
		}
	}
	@PostMapping("orderdetail")
	public ResponseEntity<Object> saveOrderDetail(@RequestBody OrderDetail od){
		OrderDetail dataOD = this.reposOD.save(od);
		if(dataOD.equals(od)) {
			return new ResponseEntity<Object>("Data saved successfully", HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("Data saved unsuccessful", HttpStatus.NO_CONTENT);
		}
	}
}
