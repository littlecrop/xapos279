package com.xsisacademy.bootcamp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xsisacademy.bootcamp.model.Category;
import com.xsisacademy.bootcamp.model.Variant;
import com.xsisacademy.bootcamp.repository.CategoryRepository;
import com.xsisacademy.bootcamp.repository.VariantRepository;

@Controller
@RequestMapping("/variant/")
public class VariantController {

	@Autowired
	private VariantRepository vRepo;
	
	@Autowired
	private CategoryRepository cRepo;

	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView v = new ModelAndView("variant/index");
		List<Variant> listVariant = this.vRepo.findAll();
		v.addObject("listvariant", listVariant);
		return v;
	}

	@GetMapping("indexvapi")
	public ModelAndView variantAPI() {
		ModelAndView v = new ModelAndView("variant/indexvapi");
		return v;
	}

	@GetMapping("addform")
	public ModelAndView addform() {
		ModelAndView v = new ModelAndView("variant/addform");
		Variant variant = new Variant();
		v.addObject("variant", variant);
		List<Category> listCategory = this.cRepo.findAll();
		v.addObject("listcategory", listCategory);
		return v;
	}

	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Variant variant, BindingResult res) {
		if (!res.hasErrors()) {
			this.vRepo.save(variant);
			return new ModelAndView("redirect:/variant/index");
		} else {
			return new ModelAndView("redirect:/variant/index");
		}
	}

	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView v = new ModelAndView("variant/addform");
		Variant variant = this.vRepo.findById(id).orElse(null);
		v.addObject("variant", variant);
		List<Category> listCategory = this.cRepo.findAll();
		v.addObject("listcategory", listCategory);
		return v;
	}

	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		if (id != null) {
			this.vRepo.deleteById(id);
		}
		return new ModelAndView("redirect:/variant/index");
	}
}
