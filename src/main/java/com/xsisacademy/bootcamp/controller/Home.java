package com.xsisacademy.bootcamp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class Home {

	@GetMapping("index")
	public String index() {
		return "index";
	}
	
	@GetMapping("formProfile")
	public String formProfile() {
		return "formProfile";
	}
	
	@GetMapping("Calculator1")
	public String kalkulator() {
		return "Calculator1.html";
	}
	
	@GetMapping("Calculator2")
	public String kalkulator2() {
		return "Calculator2.html";
	}
	
}
