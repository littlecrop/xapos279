package com.xsisacademy.bootcamp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.bootcamp.model.Variant;
import com.xsisacademy.bootcamp.repository.VariantRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class APIvariantcontroller {

	@Autowired
	public VariantRepository reposV;

	@GetMapping("variant")
	public ResponseEntity<List<Variant>> getAllVariant() {
		try {
			List<Variant> variant = this.reposV.findAll();
			return new ResponseEntity<>(variant, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("add/variant")
	public ResponseEntity<Object> saveVariant(@RequestBody Variant variant) {
		variant.setStatus(true);
		Variant variantdata = this.reposV.save(variant);
		if (variantdata.equals(variant)) {
			return new ResponseEntity<Object>("Data Saved Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Data Saved Unseccessfully", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("variant/{id}")
	public ResponseEntity<List<Variant>> getVariantById(@PathVariable("id") Long id){
		try {
			Optional<Variant> variant = this.reposV.findById(id);
			if (variant.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(variant, HttpStatus.OK);
				return rest;
			}else {
				return ResponseEntity.notFound().build();
			}
		}catch(Exception e) {
			return new ResponseEntity<List<Variant>>(HttpStatus.NO_CONTENT);
		}
		}

	@PutMapping("edit/variant/{id}")
	public ResponseEntity<Object> updateVariant(@RequestBody Variant variant, @PathVariable("id") Long id){
		try {
			Optional<Variant> variantdata = this.reposV.findById(id);
			if(variantdata.isPresent()) {
				variant.setId(id);
				variant.setStatus(true);
				this.reposV.save(variant);
				ResponseEntity rest = new ResponseEntity<>("Data Updated Successfully", HttpStatus.OK);
				return rest;
			}else {
				return ResponseEntity.notFound().build();
			}
		}catch(Exception e) {
			return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
		}
	}
	
	@PutMapping("delete/variant/{id}")
	public ResponseEntity<Object> daleteVariant(@PathVariable("id") Long id){
		Optional<Variant> variantdata = this.reposV.findById(id);
		if(variantdata.isPresent()) {
			Variant variant = new Variant();
			variant.setId(id);
			this.reposV.deleteVariantById(id);
			ResponseEntity rest = new ResponseEntity<>("Data Deleted Successfully", HttpStatus.OK);
			return rest;
		}else {
			return ResponseEntity.notFound().build();
		}
	}
}
