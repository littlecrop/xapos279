package com.xsisacademy.bootcamp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.bootcamp.model.Category;
import com.xsisacademy.bootcamp.repository.CategoryRepository;
import com.xsisacademy.bootcamp.repository.VariantRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class APIcategorycontroller {

	@Autowired
	public CategoryRepository reposC;

	@GetMapping("category")
	public ResponseEntity<List<Category>> getAllCategory() {
		try {
			List<Category> category = this.reposC.findAll();
			return new ResponseEntity<>(category, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("add/category")
	public ResponseEntity<Object> saveCategory(@RequestBody Category category) {
		category.setStatus(true);
		Category categoryData = this.reposC.save(category);
		if (categoryData.equals(category)) {
			return new ResponseEntity<Object>("Data Saved Successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Data Saved Unsuccessfully", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("category/{id}")
	public ResponseEntity<List<Category>> getCategoryById(@PathVariable("id") Long id) {
		try {
			Optional<Category> category = this.reposC.findById(id);
			if (category.isPresent()) {
				ResponseEntity rest = new ResponseEntity<>(category, HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<List<Category>>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("edit/category/{id}")
	public ResponseEntity<Object> updateCategory(@RequestBody Category category, @PathVariable("id") Long id) {
		try {
			Optional<Category> categorydata = this.reposC.findById(id);
			if (categorydata.isPresent()) {
				category.setId(id);
				this.reposC.save(category);
				ResponseEntity rest = new ResponseEntity<>("Updated Berhasil", HttpStatus.OK);
				return rest;
			} else {
				return ResponseEntity.notFound().build();
			}
		} catch (Exception e) {
			return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("delete/category/{id}")
	public ResponseEntity<Object> deleteCategory(@PathVariable("id") Long id) {
		Optional<Category> categorydata = this.reposC.findById(id);
		if (categorydata.isPresent()) {
			Category category = new Category();
			category.setId(id);
			this.reposC.deleteCategoryById(id);
			ResponseEntity rest = new ResponseEntity<>("Deleted Succes", HttpStatus.OK);
			return rest;
		} else {
			return ResponseEntity.notFound().build();
		}

	}
}
