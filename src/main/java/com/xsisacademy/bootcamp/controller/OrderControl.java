package com.xsisacademy.bootcamp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/order/")
public class OrderControl {

	@GetMapping("indexorderapi")
	public ModelAndView orderAPI() {
		ModelAndView v = new ModelAndView("order/indexorderapi");
		return v;
	}
}
