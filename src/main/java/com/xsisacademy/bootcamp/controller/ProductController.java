package com.xsisacademy.bootcamp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.xsisacademy.bootcamp.model.Product;
import com.xsisacademy.bootcamp.model.Variant;
import com.xsisacademy.bootcamp.repository.ProductRepository;
import com.xsisacademy.bootcamp.repository.VariantRepository;

@Controller
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductRepository pRepo;

	@Autowired
	private VariantRepository vRepo;

	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView v = new ModelAndView("product/index");
		List<Product> listProduct = this.pRepo.findAll();
		v.addObject("listproduct", listProduct);
		return v;
	}

	@GetMapping("indexproductapi")
	public ModelAndView productAPI() {
		ModelAndView v = new ModelAndView("product/indexproductapi");
		return v;
	}

	@GetMapping("formaddproduct")
	public ModelAndView formAddProduct() {
		ModelAndView v = new ModelAndView("product/formaddproduct");
		Product product = new Product();
		v.addObject("product", product);
		List<Variant> listVariant = this.vRepo.findAll();
		v.addObject("listvariant", listVariant);
		return v;
	}

	@PostMapping("save")
	public ModelAndView save(@ModelAttribute Product product, BindingResult res) {
		if (!res.hasErrors()) {
			this.pRepo.save(product);
			return new ModelAndView("redirect:/product/index");
		} else {
			return new ModelAndView("redirect:/product/index");
		}
	}

	@GetMapping("edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView v = new ModelAndView("product/formaddproduct");
		Product product = this.pRepo.findById(id).orElse(null);
		v.addObject("product", product);
		List<Variant> listVariant = this.vRepo.findAll();
		v.addObject("listvariant", listVariant);
		return v;
	}

	@GetMapping("delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		if (id != null) {
			this.pRepo.deleteById(id);
		}
		return new ModelAndView("redirect:/product/index");
	}
}
