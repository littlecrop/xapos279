package com.xsisacademy.bootcamp.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xsisacademy.bootcamp.model.OrderHeader;
import com.xsisacademy.bootcamp.repository.OrderHeaderRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/")
public class APIorderheadercontoller {

	@Autowired
	public OrderHeaderRepository reposOH;
	
	@GetMapping("view/orderheader")
	public ResponseEntity<List<OrderHeader>> getAllHeader(){
		try {
			List<OrderHeader> oh = this.reposOH.findAll();
			return new ResponseEntity<>(oh, HttpStatus.OK);
		}catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
	}

	@PostMapping("orderheader")
	public ResponseEntity<Object> saveOH(@RequestBody OrderHeader oh) {
		String timeDec = String.valueOf(System.currentTimeMillis());
		oh.setReference(timeDec);
		OrderHeader ohData = this.reposOH.save(oh);

		if (ohData.equals(oh)) {
			return new ResponseEntity<Object>("Data saved successfully", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>("Data saved failure", HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("orderheadergetmaxid")
	public ResponseEntity<Long> getMaxOrderHeader() {
		try {
			Long orderHeader = this.reposOH.getMaxOrderHeader();
			return new ResponseEntity<Long>(orderHeader, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Long>(HttpStatus.NO_CONTENT);
		}
	}

	@PutMapping("done/orderheader")
	public ResponseEntity<Object> doneProccesss(@RequestBody OrderHeader oh) {
		Long id = oh.getId();
		Optional<OrderHeader> dataOH = this.reposOH.findById(id);
		if (dataOH.isPresent()) {
			oh.setId(id);
			this.reposOH.save(oh);
			return new ResponseEntity<Object>("Order Success", HttpStatus.OK);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
}
