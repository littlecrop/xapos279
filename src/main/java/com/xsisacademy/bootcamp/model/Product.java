package com.xsisacademy.bootcamp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "product_code")
	private String pCode;
	
	@Column(name = "product_name")
	private String pName;

	@Column(name = "product_description")
	private String pDescription;
	
	@Column(name = "product_price")
	private Long pPrice;
	
	@Column(name = "product_stock")
	private Long pStock;
	
	@Column(name = "variant_id")
	private Long vId;
	
	@ManyToOne
	@JoinColumn(name = "variant_id", insertable = false, updatable = false)
	public Variant variant;
	
	@Column(name = "is_active")
	private Boolean pStatus;

	public Boolean getpStatus() {
		return pStatus;
	}

	public void setpStatus(Boolean pStatus) {
		this.pStatus = pStatus;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getpCode() {
		return pCode;
	}

	public void setpCode(String pCode) {
		this.pCode = pCode;
	}

	public String getpName() {
		return pName;
	}

	public void setpName(String pName) {
		this.pName = pName;
	}

	public String getpDescription() {
		return pDescription;
	}

	public void setpDescription(String pDescription) {
		this.pDescription = pDescription;
	}

	public Long getpPrice() {
		return pPrice;
	}

	public void setpPrice(Long pPrice) {
		this.pPrice = pPrice;
	}

	public Long getpStock() {
		return pStock;
	}

	public void setpStock(Long pStock) {
		this.pStock = pStock;
	}

	public Long getvId() {
		return vId;
	}

	public void setvId(Long vId) {
		this.vId = vId;
	}

	public Variant getVariant() {
		return variant;
	}

	public void setVariant(Variant variant) {
		this.variant = variant;
	}
	
	
}
