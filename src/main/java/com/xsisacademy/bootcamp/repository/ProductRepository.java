package com.xsisacademy.bootcamp.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.xsisacademy.bootcamp.model.Product;
import com.xsisacademy.bootcamp.model.Variant;

public interface ProductRepository extends JpaRepository<Product, Long> {
	
	@Query("FROM Product")
	public List<Product>findByProduct();
	
	@Modifying
	@Query(value = "UPDATE Product p SET is_active = false WHERE p.id = ?1", nativeQuery = true)
	@Transactional
	public void deleteProductById(Long id);
	
	@Query(value = "SELECT * FROM Variant v JOIN Category c ON c.id = v.category_id WHERE v.category_id = ?1", nativeQuery = true)
	public List<Variant> findVariantByCategory(Long id);
	

}
